class ApplicationMailer < ActionMailer::Base
  default from: 'support@elearn-plat.herokuapp.com'
  layout 'mailer'
end
