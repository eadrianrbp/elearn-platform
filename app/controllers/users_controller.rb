class UsersController < ApplicationController
  def index
    #@users = User.all.order(created_at: :desc)

    @query = User.ransack(params[:q])
    @users = @query.result(distinct: true)
  end
end