# README

## Local Configuration
* Database
  - pg_lsclusters
  - pg_ctlcluster 14 main start
  - rake db:create
  - bin/rails db:migrate RAILS_ENV=development
  - rake db:seed

* Run Locally (Daily)
  ```bash
  pro_dock
  docker-rails6
      cd L-rails/elearn
      bundle
      rails s -b '0.0.0.0' -p 3004
      localhost:3004
      # admin@example.com - admin@example.com
  ```

## Deployment Configuration
- deploy-heroku
  - git push heroku main
  - heroku run rake db:migrate
  - heroku run rake db:seed

## Tests

